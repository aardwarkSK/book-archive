package appts.model.exception;

import java.text.MessageFormat;

public class SectionNotFoundException extends RuntimeException{
    public SectionNotFoundException(Integer id) {
        super(MessageFormat.format("Could not find section with id: {0}", id));
    }
}
