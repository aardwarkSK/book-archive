package appts.service;

import appts.model.Book;
import appts.model.Section;
import appts.model.dto.SectionDto;
import appts.model.exception.BookIsAlreadyInDifferentSectionException;
import appts.model.exception.SectionNotFoundException;
import appts.repository.SectionJpaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class SectionService {

    @Autowired
    private SectionJpaRepository repository;

    @Autowired
    private BookService bookService;

    @Autowired
    private ModelMapper modelMapper;

    public List<SectionDto> findAllSections() {
        return convertEntityListToDtoList(repository.findAll());
    }

    public List<SectionDto> myFindAllSections() {
        return convertEntityListToDtoList(repository.findAllSections());
    }

    // testing with FetchType eager/lazy
    public String fetchTest() {
        List<Section> sections =  repository.findAll();
//        List<Section> sections =  repository.findAllSections();
        for (Section s: sections) {
            List<Book> books = s.getBooks();
            for (Book b: books) {
                System.out.println(b.toString());
            }
        }
        return "fetch test";
    }


    public SectionDto findSectionById(Integer id) {
        return convertEntityToDto(repository.findById(id).orElseThrow(() ->
                new SectionNotFoundException(id)));
    }

    public SectionDto addSection(SectionDto sectionDto) {
        return convertEntityToDto(repository.save(convertDtoToEntity(sectionDto)));
    }

    public String deleteSection(Integer id) {
        if (repository.existsById(id)){
            repository.deleteById(id);
        }
        else {
            throw new SectionNotFoundException(id);
        }
        return "Section deleted";
    }

    public SectionDto updateSection(SectionDto sectionDto) {
        Section existingSection;
        if (repository.existsById(sectionDto.getSectionId())) {
            existingSection = convertDtoToEntity(sectionDto);
        }
        else {
            throw new SectionNotFoundException(sectionDto.getSectionId());
        }
        return convertEntityToDto(repository.saveAndFlush(existingSection));
    }

    @Transactional
    public SectionDto addBookToSection(Integer sectionId, Integer bookId) {
        Section section = convertDtoToEntity(findSectionById(sectionId));
        Book book = bookService.findBookById(bookId);
        if (Objects.nonNull(book.getSection())) {
            throw new BookIsAlreadyInDifferentSectionException(book.getTitle(), book.getSection().getSectionId());
        }
        section.addBook(book);
        book.setSection(section);
        return convertEntityToDto(section);
    }

    @Transactional
    public String removeBookFromSection(Integer sectionId, Integer bookId) {
        Section section = convertDtoToEntity(findSectionById(sectionId));
        Book book = bookService.findBookById(bookId);
        section.removeBook(book);
        return "Book removed from Section";
    }

    //   ---------------------------------------- entity/dto conversions ------------------------------------
    private SectionDto convertEntityToDto(Section section) {
        return modelMapper.map(section, SectionDto.class);
    }

    private Section convertDtoToEntity(SectionDto sectionDto) {
        return modelMapper.map(sectionDto, Section.class);
    }

    private List<SectionDto> convertEntityListToDtoList(List<Section> sections) {
        List<SectionDto> sectionDtos = new ArrayList<>();
        for (Section s: sections) {
            sectionDtos.add(convertEntityToDto(s));
        }
        return sectionDtos;
    }

    private List<Section> convertDtoListToEntityList(List<SectionDto> sectionDtos) {
        List<Section> sections = new ArrayList<>();
        for (SectionDto dto : sectionDtos) {
            sections.add(convertDtoToEntity(dto));
        }
        return sections;
    }
}
