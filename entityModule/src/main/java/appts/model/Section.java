package appts.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sections")
public class Section {

    @Id
    @Column(name = "sectionId")
    private int sectionId;
    @Column(name = "genre")
    String genre;

    @OneToMany(mappedBy = "section", fetch = FetchType.LAZY)
    List<Book> books;

    public Section() {
    }

    public Section(int sectionId, String genre) {
        this.sectionId = sectionId;
        this.genre = genre;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(Book book) {
        this.books.remove(book);
    }
}
