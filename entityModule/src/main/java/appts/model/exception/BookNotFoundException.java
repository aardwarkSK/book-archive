package appts.model.exception;

import java.text.MessageFormat;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(Integer id) {
        super(MessageFormat.format("Could not find book with id: {0}", id));
    }

    public BookNotFoundException(String title) {
        super(MessageFormat.format("Could not find book: {0}", title));
    }
}
