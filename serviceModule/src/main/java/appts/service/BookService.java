package appts.service;

import appts.model.dto.AuthorCountDTO;
import appts.model.Book;
import appts.model.dto.BookDto;
import appts.model.exception.BookNotFoundException;
import appts.repository.BookJpaRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookJpaRepository repository;

    @Autowired
    private ModelMapper modelMapper;

    public BookDto saveBook(BookDto bookDto) {
        return convertEntityToDto(repository.save(convertDtoToEntity(bookDto)));
    }

    public List<BookDto> saveBooks(List<BookDto> bookDtos){
        return convertEntityListToDtoList(repository.saveAll(this.convertDtoListToEntityList(bookDtos)));
    }

    public List<BookDto> findAll() {
        return convertEntityListToDtoList(repository.findAll());
    }

    // for SectionService
    public Book findBookById(Integer bookId) {
        return repository.findById(bookId).orElseThrow(() ->
                new BookNotFoundException(bookId));

    }

    // for controller
    public BookDto findById(Integer bookId) {
        return convertEntityToDto(findBookById(bookId));
    }

    public BookDto findByTitle(String title) {
        return convertEntityToDto(repository.findByTitle(title)
                .orElseThrow(() -> new BookNotFoundException(title)));
    }

    public String deleteBook(Integer bookId) {
        if(repository.existsById(bookId)) {
            repository.deleteById(bookId);
        }
        return "Book deleted";
    }

    public BookDto updateBook(BookDto bookDto) {
        Book existingBook;
        if (repository.existsById(bookDto.getBookId())) {
            existingBook = convertDtoToEntity(bookDto);
        }
        else {
            throw new BookNotFoundException(bookDto.getBookId());
        }
        return convertEntityToDto(repository.saveAndFlush(existingBook));
    }

    public List<BookDto> pageAllBooks(Integer pageNum, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by(sortBy));

        Page<Book> pagedResult = repository.findAll(pageable);

        if(pagedResult.hasContent()) {
            return convertEntityListToDtoList(pagedResult.getContent());
        } else {
            return new ArrayList<BookDto>();
        }
    }

    public List<AuthorCountDTO> countBooksPerAuthor() {
        return repository.countBooksPerAuthor();
    }

    public List<BookDto> myPageAllBooks(Integer pageNum, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNum, pageSize, Sort.by(sortBy));

        Page<Book> pagedResult = repository.myPageAllBooks(pageable);

        if(pagedResult.hasContent()) {
            return convertEntityListToDtoList(pagedResult.getContent());
        } else {
            return new ArrayList<BookDto>();
        }
    }

    public Integer countByAuthor(String author) {
        return repository.countByAuthor(author);
    }

    public List<String> selectDistinctAuthors() {
        return repository.selectDistinctAuthors();
    }

    //   ---------------------------------------- entity/dto conversions ------------------------------------
    private BookDto convertEntityToDto(Book book) {
        return modelMapper.map(book, BookDto.class);
    }

    private Book convertDtoToEntity(BookDto bookDto) {
        return modelMapper.map(bookDto, Book.class);
    }

    private List<BookDto> convertEntityListToDtoList(List<Book> books) {
        List<BookDto> bookDtos = new ArrayList<>();
        for (Book b: books) {
            bookDtos.add(convertEntityToDto(b));
        }
        return bookDtos;
    }

    private List<Book> convertDtoListToEntityList(List<BookDto> bookDtos) {
        List<Book> books = new ArrayList<>();
        for (BookDto dto : bookDtos) {
            books.add(convertDtoToEntity(dto));
        }
        return books;
    }
}
