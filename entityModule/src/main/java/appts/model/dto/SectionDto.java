package appts.model.dto;

import appts.model.Book;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

public class SectionDto {
    private int sectionId;
    String genre;
    @JsonIgnoreProperties("section")
    List<BookDto> books;

    public SectionDto() {
    }

    public SectionDto(int sectionId, String genre, List<BookDto> books) {
        this.sectionId = sectionId;
        this.genre = genre;
        this.books = books;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public List<BookDto> getBooks() {
        return books;
    }

    public void setBooks(List<BookDto> books) {
        this.books = books;
    }
}
