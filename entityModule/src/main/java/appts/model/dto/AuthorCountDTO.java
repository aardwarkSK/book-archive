package appts.model.dto;


public class AuthorCountDTO {
    private String author;
    private Long numOfBooks;

    public AuthorCountDTO() {
    }

    public AuthorCountDTO(String author, Long numOfBooks) {
        this.author = author;
        this.numOfBooks = numOfBooks;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getNumOfBooks() {
        return numOfBooks;
    }

    public void setNumOfBooks(Long numOfBooks) {
        this.numOfBooks = numOfBooks;
    }
}
