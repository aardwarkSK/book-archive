package appts.repository;

import appts.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectionJpaRepository extends JpaRepository<Section, Integer> {
    @Query(value = "SELECT DISTINCT s FROM Section s JOIN FETCH s.books b")
    List<Section> findAllSections();
}
