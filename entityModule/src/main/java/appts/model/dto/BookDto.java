package appts.model.dto;

import appts.model.Section;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class BookDto {
    private int bookId;
    private String title;
    private String author;
    @JsonIgnoreProperties("books")
    private SectionDto section;

    public BookDto() {
    }

    public BookDto(int bookId, String title, String author, SectionDto section) {
        this.bookId = bookId;
        this.title = title;
        this.author = author;
        this.section = section;
    }

//    public BookDto(int bookId, String title, String author) {
//        this.bookId = bookId;
//        this.title = title;
//        this.author = author;
//    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public SectionDto getSection() {
        return section;
    }

    public void setSection(SectionDto section) {
        this.section = section;
    }
}
