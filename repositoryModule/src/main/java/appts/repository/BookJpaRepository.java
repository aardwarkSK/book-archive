package appts.repository;

import appts.model.dto.AuthorCountDTO;
import appts.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookJpaRepository extends JpaRepository<Book, Integer> {
    Optional<Book> findByTitle(String title);


    @Query(value = "SELECT new appts.model.dto.AuthorCountDTO(b.author, COUNT(b)) FROM Book b GROUP BY b.author")
    List<AuthorCountDTO> countBooksPerAuthor();

    @Query(value = "SELECT b FROM Book b ORDER BY bookId")
    Page<Book> myPageAllBooks(Pageable pageable);

    Integer countByAuthor(String author);

    @Query(value = "SELECT DISTINCT b.author FROM Book b")
    List<String> selectDistinctAuthors();
}
