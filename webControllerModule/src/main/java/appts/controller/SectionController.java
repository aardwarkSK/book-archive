package appts.controller;

import appts.model.Section;
import appts.model.dto.SectionDto;
import appts.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SectionController {
    @Autowired
    SectionService service;


    @GetMapping("/findAllSections")
    public List<SectionDto> findAllSections() {
        return service.findAllSections();
    }

    @GetMapping("/myFindAllSections")
    public List<SectionDto> myFindAllSections() {
        return service.myFindAllSections();
    }

    @GetMapping("/fetchTest")
    public String fetchTest() {
        return service.fetchTest();
    }

    @GetMapping("/findSectionById/{id}")
    public SectionDto findSectionById(@PathVariable Integer id) {
        return service.findSectionById(id);
    }

    @PostMapping("/addSection")
    public SectionDto addSection(@RequestBody SectionDto sectionDto) {
        return service.addSection(sectionDto);
    }

    @PostMapping("/addBookToSection/section/{sectionId}/book/{bookId}")
    public SectionDto addBookToSection(@PathVariable Integer sectionId,
                                       @PathVariable Integer bookId) {
        return service.addBookToSection(sectionId, bookId);
    }

    @DeleteMapping("/deleteSection/{id}")
    public String deleteSection(@PathVariable Integer id) {
        return service.deleteSection(id);
    }

    @DeleteMapping("/removeBookFromSection/section/{sectionId}/book/{bookId}")
    public String removeBookFromSection(@PathVariable Integer sectionId,
                                        @PathVariable Integer bookId) {
        return service.removeBookFromSection(sectionId, bookId);
    }

    @PutMapping("/updateSection")
    public SectionDto updateSection(@RequestBody SectionDto sectionDto) {
        return service.updateSection(sectionDto);
    }
}
