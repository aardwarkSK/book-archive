package appts.model.exception;

import java.text.MessageFormat;

public class BookIsAlreadyInDifferentSectionException extends RuntimeException{
    public BookIsAlreadyInDifferentSectionException(String title, Integer sectionId) {
        super(MessageFormat.format("Book: \"{0}\" is already in section: {1}", title, sectionId));
    }
}
