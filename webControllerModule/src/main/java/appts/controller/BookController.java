package appts.controller;

import appts.model.dto.AuthorCountDTO;
import appts.model.Book;
import appts.model.dto.BookDto;
import appts.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    private BookService service;

    @PostMapping("/addBook")
    public BookDto addBook(@RequestBody BookDto bookDto) {
        return service.saveBook(bookDto);
    }

    @PostMapping("/addBooks")
    public List<BookDto> addBook(@RequestBody List<BookDto> bookDtos) {
        return service.saveBooks(bookDtos);
    }

    @GetMapping("/findAllBooks")
    public List<BookDto> findAllBooks() {
        return service.findAll();
    }

    @GetMapping("/findBookById/{bookId}")
    public BookDto findBookById(@PathVariable int bookId) {
        return service.findById(bookId);
    }

    @GetMapping("/findBookByTitle/{title}")
    public BookDto findBookByTitle(@PathVariable String title) {
        return service.findByTitle(title);
    }

    @PutMapping("/update")
    public BookDto updateBook(@RequestBody BookDto bookDto) {
        return service.updateBook(bookDto);
    }

    @DeleteMapping("/delete/{bookId}")
    public  String deleteBook(@PathVariable int bookId) {
        return service.deleteBook(bookId);
    }

    @GetMapping("/pageAllBooks")
    public ResponseEntity<List<BookDto>> pageAllBooks(
            @RequestParam(defaultValue = "0") Integer pageNum,
            @RequestParam(defaultValue = "3") Integer pageSize,
            @RequestParam(defaultValue = "bookId") String sortBy
    ) {
        List<BookDto> bookDtos = service.pageAllBooks(pageNum, pageSize, sortBy);

        return new ResponseEntity<List<BookDto>>(bookDtos, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/countBooksPerAuthor")
    public List<AuthorCountDTO> countBooksPerAuthor() {
        return service.countBooksPerAuthor();
    }

    @GetMapping("/myPageAllBooks")
    public ResponseEntity<List<BookDto>> myPageAllBooks(
            @RequestParam(defaultValue = "0") Integer pageNum,
            @RequestParam(defaultValue = "3") Integer pageSize,
            @RequestParam(defaultValue = "bookId") String sortBy
    ) {
        List<BookDto> bookDtos = service.myPageAllBooks(pageNum, pageSize, sortBy);

        return new ResponseEntity<List<BookDto>>(bookDtos, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/countByAuthor/{author}")
    public Integer countAuthors(@PathVariable String author) {
        return service.countByAuthor(author);
    }

    @GetMapping("/findAllAuthors")
    public List<String> selectDistinctAuthors() {
        return service.selectDistinctAuthors();
    }

}
